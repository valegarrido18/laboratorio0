#include "Cadena.h"
#include "Proteina.h"
#include <iostream>
#include <list>
using namespace std;

/* constructores */

Proteina::Proteina (string nombre, string id)
{    
    this->nombre = nombre;
    this->id = id;
}

/* métodos get and set */
string Proteina::get_nombre() 
{
    return this->nombre;
}

string Proteina::get_id()
{
    return this->id;
}

list<Cadena> Proteina:: get_cadenas()
{
	return this->cadenas;
}        

void Proteina::set_nombre(string nombre)
{
    this->nombre = nombre;
}
        
void Proteina::set_id(string id) 
{
    this->id = id;
}

void Proteina::add_cadena(Cadena cadena)
{
	this->cadenas.push_back(cadena);
}
