#include "Cadena.h"
#include <iostream>
#include <list>

using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina 
{
    private:
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadenas;
        
    public:
        /* constructor */
        Proteina (string nombre, string id);
        void add_cadena(Cadena cadena);
        
        /* métodos get */
        string get_nombre();
        string get_id();
        list<Cadena> get_cadenas();
        
        /* métodos set */
        void set_nombre(string nombre);
        void set_id(string id);
        
};
#endif
