#include "Coordenada.h"
#include <iostream>
#include <list>

using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
    private:
        string nombre = "\0";
        int numero;
        Coordenada coordenada = coordenada;
        
    public:
        /* constructor */
        Atomo (string nombre, int numero);
        
        /* métodos get */
        string get_nombre();
        int get_numero();
		Coordenada get_coordenada();
        
        /* métodos set */
        void set_coordenada(Coordenada coordena);
        void set_nombre(string nombre);
        void set_numero(int numero);
};
#endif
