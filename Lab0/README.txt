                                                    
						                       -- Ingresos de datos proteína --

Programa el cual fue creado de acuerdo al diagrama de clase adjunto, narrado en C++ que implementa dichas clases. El programa debe permitir el ingreso  de una lista de proteínas con su información asociada.

    • EMPEZANDO

El programa consiste en darle al usuario la posibilidad de poder ingresar una proteína, su ID, la letra de la cadena, nombre aminoacido, número aminoacido, nombre atomo, número atomo, luego tomará todos los datos adjuntos y los mostrará en pantalla en su debido orden, por ultimo para finalizar el programa este preguntará si desea o no ingresar una nueva proteína, al seleccionar que si se volveran a pedir los mismo datos al usurario para volver a realizar todo denuevo, de no ser así el programa se cerrará. 

>> Funciona para una sola proteína a la vez, es necesario destacar que el programa dejará de funcionar si se ingresa una letra en donde se requieren números en el momento de pedir datos al usuario. 

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa para que este funcione. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
