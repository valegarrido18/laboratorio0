#include "Aminoacido.h"
#include <iostream>
#include <list>
#include "Atomo.h"
using namespace std;

/* constructores */

Aminoacido::Aminoacido (string nombre, int numero)
{    
    this->nombre = nombre;
    this->numero = numero;
}

/* métodos get and set */
string Aminoacido::get_nombre()
{
    return this->nombre;
}

int Aminoacido::get_numero()
{
    return this->numero;
}

list<Atomo> Aminoacido:: get_atomo()
{
	return this->atomo;
}       

void Aminoacido:: add_atomo(Atomo atomo)
{
	this -> atomo.push_back(atomo);
}

void Aminoacido::set_nombre(string nombre)
{
    this->nombre = nombre;
}
        
void Aminoacido::set_numero(int numero)
{
    this->numero = numero;
}
